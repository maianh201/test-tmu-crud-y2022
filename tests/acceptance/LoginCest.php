<?php

use Page\Acceptance\LoginPage;
use Step\Acceptance\LoginStep;

class LoginCest
{
    public function show_home_page_when_data_login_successfully(LoginStep $loginStep, LoginPage $loginPage)
    {
        $loginStep->login('hangtt@gmail.com', '123456789');
        $loginPage->checkRememberMe()->clickLoginButton();
        $loginStep->retrySee('Dashboard');
    }
}
